package ru.dmitriy.currencyconverter.presentation.converter.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.dmitriy.currencyconverter.domain.converter.ConverterInteractor
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel
import ru.dmitriy.currencyconverter.presentation.base.BasePresenter
import ru.dmitriy.currencyconverter.presentation.converter.view.ConverterView
import javax.inject.Inject

@InjectViewState
class ConverterPresenter
    @Inject constructor(private val converterInteractor: ConverterInteractor): BasePresenter<ConverterView>() {

    private var disposableCurrencies: Disposable? = null

    private fun startObservingCurrencies() {
        disposableCurrencies = getCurrencies()
    }

    private fun getCurrencies(): Disposable {
        return converterInteractor.getCurrencies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ viewState.showCurrencies(it.rates) },
                { viewState.showError() })
    }

    fun setLastCurrency(currencyModel: CurrencyModel) {
        converterInteractor.setLastCurrency(currencyModel)
        disposableCurrencies?.dispose()
        disposableCurrencies = getCurrencies()
    }

    override fun onAttach() {
        super.onAttach()
        startObservingCurrencies()
    }

    override fun onDetach() {
        super.onDetach()
        disposableCurrencies?.dispose()
        disposableCurrencies = null
    }

}
package ru.dmitriy.currencyconverter.presentation.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.dmitriy.currencyconverter.R
import ru.dmitriy.currencyconverter.di.Injector
import ru.dmitriy.currencyconverter.presentation.converter.view.ConverterFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            //probably not the best place for initializtion of component, but it's good for Single Activity apps
            Injector.getConverterComponent()
            supportFragmentManager.beginTransaction()
                .add(R.id.container, ConverterFragment())
                .commit()
        }
    }
}

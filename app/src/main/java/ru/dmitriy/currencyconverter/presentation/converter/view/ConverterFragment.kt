package ru.dmitriy.currencyconverter.presentation.converter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_currency.*
import ru.dmitriy.currencyconverter.R
import ru.dmitriy.currencyconverter.di.Injector
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel
import ru.dmitriy.currencyconverter.presentation.converter.adapter.CurrenciesAdapter
import ru.dmitriy.currencyconverter.presentation.converter.presenter.ConverterPresenter
import androidx.recyclerview.widget.DividerItemDecoration

class ConverterFragment : MvpAppCompatFragment(),
    ConverterView {

    lateinit var currenciesAdapter: CurrenciesAdapter

    @InjectPresenter
    lateinit var converterPresenter: ConverterPresenter

    @ProvidePresenter
    fun provideConverterPresenter() = Injector.getConverterComponent()?.getConverterPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        Injector.getConverterComponent()?.inject(this)
        super.onCreate(savedInstanceState)
        setupAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_currency, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupAdapter() {
        currenciesAdapter = CurrenciesAdapter(
            requireContext()
        ) {
            converterPresenter.setLastCurrency(it)
        }
    }

    private fun setupRecyclerView() {

        val linearLayoutManager = LinearLayoutManager(activity)
        currency_list.layoutManager = linearLayoutManager
        currency_list.adapter = currenciesAdapter
        val dividerItemDecoration = DividerItemDecoration(
            currency_list.context,
            linearLayoutManager.orientation
        )
        currency_list.addItemDecoration(dividerItemDecoration)
    }

    override fun showCurrencies(currencies: List<CurrencyModel>) {
        currenciesAdapter.submitList(currencies)
    }

    override fun showError() {
        Toast.makeText(activity, "Error while loading currencies", LENGTH_SHORT).show()
    }

    override fun onDetach() {
        super.onDetach()
        Injector.releaseConverterComponent()
    }
}
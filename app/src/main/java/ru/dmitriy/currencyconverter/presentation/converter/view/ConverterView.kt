package ru.dmitriy.currencyconverter.presentation.converter.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel


interface ConverterView: MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showCurrencies(currencies: List<CurrencyModel>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError()
}
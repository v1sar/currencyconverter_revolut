package ru.dmitriy.currencyconverter.presentation.converter.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.item_currency.view.*
import ru.dmitriy.currencyconverter.R
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel
import ru.dmitriy.currencyconverter.presentation.utils.view.CurrencyImage
import java.util.*

class CurrenciesAdapter(
    private val context: Context,
    private val onClickListener: (CurrencyModel) -> Unit
) : ListAdapter<CurrencyModel, CurrenciesAdapter.MyViewHolder>(
    CurrenciesDiffUtilCallback()
) {

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(getItem(position), onClickListener)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
            return
        }
        holder.updateItem(payloads.first() as Double)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(
                context
            ).inflate(R.layout.item_currency, parent, false)
        )
    }

    override fun onViewDetachedFromWindow(holder: MyViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onDetach()
    }

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val TAG = MyViewHolder::class.simpleName

        private val tvCurrencyName = view.currency_name
        private val tvCurrencyFullName = view.currency_full_name
        private val etCurrencyRate = view.currency_rate
        private val ivCurrencyImg = view.currency_image
        private var textWatcherDisposable: Disposable? = null
        private var onTextChangedListener: ((CurrencyModel) -> Unit)? = null

        fun bind(
            currencyModel: CurrencyModel,
            clickListener: (CurrencyModel) -> Unit
        ) {
            tvCurrencyName.text = currencyModel.name
            tvCurrencyFullName.text = Currency.getInstance(currencyModel.name).displayName
            ivCurrencyImg.setImageResource(CurrencyImage.from(currencyModel.name))
            view.setOnClickListener { etCurrencyRate.requestFocusFromTouch() }
            this.onTextChangedListener = clickListener

            with(etCurrencyRate) {
                setText(currencyModel.rate.toString(), TextView.BufferType.EDITABLE)
                setOnFocusChangeListener { v, hasFocus ->
                    if (!hasFocus) {
                        manageTextWatcher(false)
                        return@setOnFocusChangeListener
                    }
                    val model = CurrencyModel(tvCurrencyName.text.toString(), etCurrencyRate.text.toString().toDouble())
                    clickListener.invoke(model)
                    manageTextWatcher(true)
                }
            }
        }

        fun updateItem(currencyRate: Double) {
            if (!etCurrencyRate.isFocused) {
                etCurrencyRate.setText(currencyRate.toString(), TextView.BufferType.EDITABLE)
            }
        }

        fun manageTextWatcher(hasFocus: Boolean) {
            if (!hasFocus) {
                textWatcherDisposable?.let { textWatcherDisposable!!.dispose() }
            } else {
                textWatcherDisposable = RxTextView.textChanges(etCurrencyRate)
                    .subscribe({
                        val rate = if (etCurrencyRate.text.toString().isEmpty()) 1.0 else etCurrencyRate.text.toString()
                            .toDouble()
                        val model = CurrencyModel(tvCurrencyName.text.toString(), rate)
                        onTextChangedListener?.invoke(model)
                    },
                        {
                            Log.e(TAG, "Error with RxBindings: ${it.localizedMessage}")
                            it.printStackTrace()
                        })
            }
        }

        fun onDetach() {
            etCurrencyRate.clearFocus()
        }
    }
}
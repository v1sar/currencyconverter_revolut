package ru.dmitriy.currencyconverter.presentation.converter.adapter

import androidx.recyclerview.widget.DiffUtil
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel

class CurrenciesDiffUtilCallback: DiffUtil.ItemCallback<CurrencyModel>() {

    override fun areItemsTheSame(oldItem: CurrencyModel, newItem: CurrencyModel): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: CurrencyModel, newItem: CurrencyModel): Boolean {
        return oldItem.rate == newItem.rate
    }

    override fun getChangePayload(oldItem: CurrencyModel, newItem: CurrencyModel): Any? {
        var rate = oldItem.rate
        if (oldItem.rate != newItem.rate) {
            rate = newItem.rate
        }
        return rate
    }

}
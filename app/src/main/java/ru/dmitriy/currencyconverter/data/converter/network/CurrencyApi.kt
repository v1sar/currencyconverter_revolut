package ru.dmitriy.currencyconverter.data.converter.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.dmitriy.currencyconverter.data.converter.network.model.CurrenciesResponse

interface CurrencyApi {

    @GET("latest")
    fun getLatest(@Query("base") baseCurrency: String): Single<CurrenciesResponse>
}
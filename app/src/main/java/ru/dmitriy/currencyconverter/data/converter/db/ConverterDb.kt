package ru.dmitriy.currencyconverter.data.converter.db

import androidx.room.RoomDatabase
import androidx.room.Database
import ru.dmitriy.currencyconverter.data.converter.db.entity.CurrencyEntity


@Database(entities = [CurrencyEntity::class], version = 1)
abstract class ConverterDb: RoomDatabase() {
    abstract val currenciesDao: CurrenciesDao
}
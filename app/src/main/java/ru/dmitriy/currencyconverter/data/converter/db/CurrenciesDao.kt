package ru.dmitriy.currencyconverter.data.converter.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Single
import ru.dmitriy.currencyconverter.data.converter.db.entity.CurrencyEntity

@Dao
interface CurrenciesDao {

    @Insert
    fun insert(currencyEntity: CurrencyEntity)

    @Insert
    fun insertList(currencyList: List<CurrencyEntity>)

    @Query("DELETE FROM currencies")
    fun deleteAll()

    @Query("SELECT * FROM currencies" )
    fun getAllCurrencies() : List<CurrencyEntity>

    @Query("SELECT * FROM currencies WHERE base = :base")
    fun getByBase(base: String): List<CurrencyEntity>

    @Update
    fun updateList(currencyList: List<CurrencyEntity>): Int

    @Query("DELETE FROM currencies WHERE base = :base")
    fun deleteByBase(base: String)
}
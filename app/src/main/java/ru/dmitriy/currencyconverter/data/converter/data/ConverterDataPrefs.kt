package ru.dmitriy.currencyconverter.data.converter.data

import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel

interface ConverterDataPrefs {

    fun getLastCurrencyModel(): CurrencyModel
    fun setLastCurrencyModel(currencyModel: CurrencyModel)
    fun setLastMultiplier(double: Double)
}
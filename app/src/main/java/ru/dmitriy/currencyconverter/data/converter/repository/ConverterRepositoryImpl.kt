package ru.dmitriy.currencyconverter.data.converter.repository

import io.reactivex.Single
import ru.dmitriy.currencyconverter.data.converter.data.ConverterDataPrefs
import ru.dmitriy.currencyconverter.data.converter.db.ConverterDb
import ru.dmitriy.currencyconverter.data.converter.db.entity.CurrencyEntity
import ru.dmitriy.currencyconverter.data.converter.network.CurrencyApi
import ru.dmitriy.currencyconverter.data.converter.network.model.CurrenciesResponse
import ru.dmitriy.currencyconverter.domain.converter.ConverterRepository
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel
import javax.inject.Inject

class ConverterRepositoryImpl
@Inject constructor(
    private val currencyApi: CurrencyApi,
    private val converterDataPrefs: ConverterDataPrefs,
    private val converterDb: ConverterDb
) : ConverterRepository {

    override fun getCurrenciesFromNetwork(baseCurrency: String): Single<CurrenciesResponse> {
        return currencyApi.getLatest(baseCurrency)
    }

    override fun getCurrenciesFromDb(baseCurrency: String): List<CurrencyEntity> {
        return converterDb.currenciesDao.getByBase(baseCurrency)
    }

    override fun updateCurrenciesInDb(currencies: CurrenciesResponse) {
        val base = currencies.base
        converterDb.currenciesDao.deleteByBase(base)
        val list = currencies.rates.map {
            val name = it.key
            val rate = it.value
            CurrencyEntity(base = base, name = name, rate = rate)
        }.toMutableList()

        val lastCurrencyEntity = CurrencyEntity(
            base = base,
            name = getLastCurrencyModel().name,
            rate = getLastCurrencyModel().rate
        )
        list.add(0, lastCurrencyEntity)

        val changed = converterDb.currenciesDao.updateList(list)
        if (changed == 0) {
            converterDb.currenciesDao.insertList(list)
        }
    }

    override fun getLastCurrencyModel(): CurrencyModel {
        return converterDataPrefs.getLastCurrencyModel()
    }

    override fun setLastCurrencyModel(model: CurrencyModel) {
        converterDataPrefs.setLastCurrencyModel(model)
    }

}
package ru.dmitriy.currencyconverter.data.converter.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currencies")
data class CurrencyEntity(@PrimaryKey(autoGenerate = true) var id: Long? = null,
                          val base: String,
                          val name: String,
                          val rate: Double)
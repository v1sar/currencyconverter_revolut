package ru.dmitriy.currencyconverter.data.converter.data

import android.content.Context
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel
import javax.inject.Inject
import ru.dmitriy.currencyconverter.getDouble
import ru.dmitriy.currencyconverter.putDouble


private const val lastCurrencyModel = "LAST_CURRENCY_MODEL"
private const val lastCurrencyModelName = "LAST_CURRENCY_MODEL_NAME"
private const val lastCurrencyModelRate = "LAST_CURRENCY_MODEL_RATE"

private const val defaultCurrencyModelName = "AUD"
private const val defaultCurrencyModelRate = 1.0

class ConverterDataPrefsImpl
@Inject constructor(private val context: Context) :
    ConverterDataPrefs {

    override fun getLastCurrencyModel(): CurrencyModel {
        val sharedPreference = context.getSharedPreferences(lastCurrencyModel, Context.MODE_PRIVATE)
        return CurrencyModel(
            sharedPreference.getString(lastCurrencyModelName, defaultCurrencyModelName),
            sharedPreference.getDouble(lastCurrencyModelRate, defaultCurrencyModelRate)
        )
    }

    override fun setLastCurrencyModel(currencyModel: CurrencyModel) {
        val sharedPreference = context.getSharedPreferences(lastCurrencyModel, Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()
        editor.putString(lastCurrencyModelName, currencyModel.name)
        editor.putDouble(lastCurrencyModelRate, currencyModel.rate)
        editor.commit()
    }

    override fun setLastMultiplier(double: Double) {
        val sharedPreference = context.getSharedPreferences(lastCurrencyModel, Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()
        editor.putDouble(lastCurrencyModelRate, double)
        editor.commit()
    }
}
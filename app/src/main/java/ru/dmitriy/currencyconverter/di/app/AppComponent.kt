package ru.dmitriy.currencyconverter.di.app

import dagger.Component
import ru.dmitriy.currencyconverter.di.converter.ConverterComponent
import javax.inject.Singleton

@Component(modules = [AppModule::class, NetworkModule::class])
@Singleton
interface AppComponent {

    fun converterBuilder(): ConverterComponent.Builder

}
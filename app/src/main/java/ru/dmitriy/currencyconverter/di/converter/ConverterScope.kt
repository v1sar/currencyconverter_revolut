package ru.dmitriy.currencyconverter.di.converter

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ConverterScope
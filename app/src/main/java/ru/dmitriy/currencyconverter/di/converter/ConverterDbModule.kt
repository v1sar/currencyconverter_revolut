package ru.dmitriy.currencyconverter.di.converter

import android.content.Context
import dagger.Module
import dagger.Provides
import androidx.room.Room
import ru.dmitriy.currencyconverter.data.converter.db.ConverterDb


@Module
class ConverterDbModule {

    @Provides
    @ConverterScope
    fun provideDb(context: Context): ConverterDb {
        return Room.databaseBuilder(
            context,
            ConverterDb::class.java, "converter-database"
        ).build()
    }

}
package ru.dmitriy.currencyconverter.di.converter

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.dmitriy.currencyconverter.data.converter.network.CurrencyApi

private const val URL = "https://revolut.duckdns.org/"

@Module
class ConverterNetworkModule {

    @Provides
    @ConverterScope
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .baseUrl(URL)
            .build()
    }

    @Provides
    @ConverterScope
    fun provideCurrencyApi(retrofit: Retrofit): CurrencyApi {
        return retrofit.create(CurrencyApi::class.java)
    }

}
package ru.dmitriy.currencyconverter.di

import android.app.Application
import ru.dmitriy.currencyconverter.di.app.AppComponent
import ru.dmitriy.currencyconverter.di.app.AppModule
import ru.dmitriy.currencyconverter.di.app.DaggerAppComponent
import ru.dmitriy.currencyconverter.di.converter.ConverterComponent
import java.lang.IllegalStateException

object Injector {

    private lateinit var appComponent: AppComponent
    private var converterComponent: ConverterComponent? = null

    fun initAppComponent(app: Application) {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(app))
            .build()
    }

    fun getConverterComponent(): ConverterComponent? {
        if (!::appComponent.isInitialized) {
            throw IllegalStateException("AppComponent not initialized when ConverterComponent initialization begin")
        }
        if (converterComponent == null) {
            converterComponent = appComponent.converterBuilder().build()
        }
        return converterComponent
    }

    fun releaseConverterComponent() {
        converterComponent = null
    }

}
package ru.dmitriy.currencyconverter.di.converter

import dagger.Subcomponent
import ru.dmitriy.currencyconverter.presentation.converter.presenter.ConverterPresenter
import ru.dmitriy.currencyconverter.presentation.converter.view.ConverterFragment

@ConverterScope
@Subcomponent(modules = [ConverterMainModule::class, ConverterNetworkModule::class, ConverterDbModule::class])
interface ConverterComponent {

    fun getConverterPresenter(): ConverterPresenter

    fun inject(converterFragment: ConverterFragment)

    @Subcomponent.Builder
    interface Builder {
        fun build(): ConverterComponent
    }

}
package ru.dmitriy.currencyconverter.di.converter

import dagger.Binds
import dagger.Module
import ru.dmitriy.currencyconverter.data.converter.data.ConverterDataPrefsImpl
import ru.dmitriy.currencyconverter.domain.converter.ConverterInteractor
import ru.dmitriy.currencyconverter.domain.converter.ConverterInteractorImpl
import ru.dmitriy.currencyconverter.data.converter.repository.ConverterRepositoryImpl
import ru.dmitriy.currencyconverter.data.converter.data.ConverterDataPrefs
import ru.dmitriy.currencyconverter.domain.converter.ConverterRepository

@Module
abstract class ConverterMainModule {

    @Binds
    @ConverterScope
    abstract fun provideConverterInteractor(converterInteractorImpl: ConverterInteractorImpl): ConverterInteractor

    @Binds
    @ConverterScope
    abstract fun provideConverterRepository(converterRepositoryImpl: ConverterRepositoryImpl): ConverterRepository

    @Binds
    @ConverterScope
    abstract fun provideConverterDataprefs(converterDataPrefsImpl: ConverterDataPrefsImpl): ConverterDataPrefs

}
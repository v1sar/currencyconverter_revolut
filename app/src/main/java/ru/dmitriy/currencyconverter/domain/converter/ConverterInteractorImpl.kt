package ru.dmitriy.currencyconverter.domain.converter

import io.reactivex.Observable
import ru.dmitriy.currencyconverter.data.converter.db.entity.CurrencyEntity
import ru.dmitriy.currencyconverter.data.converter.network.model.CurrenciesResponse
import ru.dmitriy.currencyconverter.domain.converter.model.CurrenciesDomainModel
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ConverterInteractorImpl
@Inject constructor(private val converterRepository: ConverterRepository) : ConverterInteractor {

    override fun getCurrencies(): Observable<CurrenciesDomainModel> {
        return Observable.concat(getCurrenciesFromDb(), getCurrenciesFromNetwork())
    }

    private fun getCurrenciesFromDb(): Observable<CurrenciesDomainModel> {
        return Observable.create<CurrenciesDomainModel> {
            val list = converterRepository.getCurrenciesFromDb(getLastBaseCurrency().name)
            if (list.isEmpty()) {
                it.onComplete()
            }
            it.onNext(multipleAllValues(mapToDomainModel(list)))
            it.onComplete()
        }
    }

    private fun getCurrenciesFromNetwork(): Observable<CurrenciesDomainModel> {
        return Observable.interval(0, 1, TimeUnit.SECONDS)
            .flatMapSingle { converterRepository.getCurrenciesFromNetwork(getLastBaseCurrency().name) }
            .retryWhen { it.delay(1, TimeUnit.SECONDS) }
            .map {
                multipleAllValues(
                    mapToDomainModelAndSave(it)
                )
            }
    }

    private fun mapToDomainModelAndSave(it: CurrenciesResponse): List<CurrencyModel> {
        converterRepository.updateCurrenciesInDb(it)
        val currencyList = it.rates.map {
            val currencyName = it.key
            val currencyRate = it.value
            CurrencyModel(currencyName, currencyRate)
        }.toMutableList()
        currencyList.add(0, getLastBaseCurrency())
        return currencyList
    }

    private fun mapToDomainModel(it: List<CurrencyEntity>): List<CurrencyModel> {
        val currencyList = it.map {
            val currencyName = it.name
            val currencyRate = it.rate
            CurrencyModel(currencyName, currencyRate)
        }.toMutableList()
        val lastCurrency = getLastBaseCurrency()
        val list = currencyList.filter { it.name != lastCurrency.name }.toMutableList()
        list.add(0, lastCurrency)
        return list
    }

    private fun multipleAllValues(list: List<CurrencyModel>): CurrenciesDomainModel {
        var firstElem = true
        return CurrenciesDomainModel(list.map {
            val currencyName = it.name
            val currencyRate = if (firstElem) {
                firstElem = false
                it.rate
            } else {
                val number = it.rate * getLastBaseCurrency().rate
                val roundedNumber = Math.round(number * 1000.0) / 1000.0
                roundedNumber
            }
            CurrencyModel(currencyName, currencyRate)
        })
    }

    private fun getLastBaseCurrency(): CurrencyModel {
        return converterRepository.getLastCurrencyModel()
    }

    override fun setLastCurrency(currencyModel: CurrencyModel) {
        converterRepository.setLastCurrencyModel(currencyModel)
    }

}
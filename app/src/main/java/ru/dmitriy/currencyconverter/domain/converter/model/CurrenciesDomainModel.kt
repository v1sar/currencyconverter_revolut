package ru.dmitriy.currencyconverter.domain.converter.model

data class CurrenciesDomainModel(
    val rates: List<CurrencyModel>
)


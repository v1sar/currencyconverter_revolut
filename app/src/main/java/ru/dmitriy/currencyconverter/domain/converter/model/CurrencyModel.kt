package ru.dmitriy.currencyconverter.domain.converter.model

data class CurrencyModel(val name: String, var rate: Double)
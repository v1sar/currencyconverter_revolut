package ru.dmitriy.currencyconverter.domain.converter

import io.reactivex.Single
import ru.dmitriy.currencyconverter.data.converter.db.entity.CurrencyEntity
import ru.dmitriy.currencyconverter.data.converter.network.model.CurrenciesResponse
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel

interface ConverterRepository {

    fun getCurrenciesFromNetwork(baseCurrency: String): Single<CurrenciesResponse>
    fun getCurrenciesFromDb(baseCurrency: String): List<CurrencyEntity>

    fun updateCurrenciesInDb(currencies: CurrenciesResponse)

    fun getLastCurrencyModel(): CurrencyModel
    fun setLastCurrencyModel(model: CurrencyModel)
}
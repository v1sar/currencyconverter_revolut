package ru.dmitriy.currencyconverter.domain.converter

import io.reactivex.Observable
import ru.dmitriy.currencyconverter.domain.converter.model.CurrenciesDomainModel
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel

interface ConverterInteractor {

    fun getCurrencies(): Observable<CurrenciesDomainModel>
    fun setLastCurrency(currencyModel: CurrencyModel)
}
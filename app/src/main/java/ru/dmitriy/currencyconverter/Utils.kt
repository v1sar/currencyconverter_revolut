package ru.dmitriy.currencyconverter

import android.content.SharedPreferences
import android.content.SharedPreferences.Editor

fun Editor.putDouble(key: String, value: Double): Editor {
    return this.putLong(key, java.lang.Double.doubleToRawLongBits(value))
}

fun SharedPreferences.getDouble(key: String, defaultValue: Double): Double {
    return java.lang.Double.longBitsToDouble(this.getLong(key, java.lang.Double.doubleToLongBits(defaultValue)))
}
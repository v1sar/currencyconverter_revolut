package ru.dmitriy.currencyconverter

import android.app.Application
import ru.dmitriy.currencyconverter.di.Injector
import ru.dmitriy.currencyconverter.di.app.AppComponent
import ru.dmitriy.currencyconverter.di.app.DaggerAppComponent

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.initAppComponent(this)
    }

}
package ru.dmitriy.currencyconverter

import ru.dmitriy.currencyconverter.data.converter.db.entity.CurrencyEntity
import ru.dmitriy.currencyconverter.data.converter.network.model.CurrenciesResponse
import ru.dmitriy.currencyconverter.domain.converter.model.CurrenciesDomainModel
import ru.dmitriy.currencyconverter.domain.converter.model.CurrencyModel

object Stub {

    val rubCode = "RUB"
    val eurCode = "EUR"
    val usdCode = "USD"
    val rubRate = 90.0
    val eurRate = 2.0
    val usdRate = 3.5

    val eurRateNetwork = 5.0
    val usdRateNetwork = 13.5

    val listOfCurrenciesEntities = listOf(CurrencyEntity(base = rubCode, name = eurCode, rate = eurRate),
        CurrencyEntity(base = rubCode, name = usdCode, rate = usdRate)
    )

    val currencyResponseStub = CurrenciesResponse(
        rubCode, "", mapOf(eurCode to eurRateNetwork, usdCode to usdRateNetwork)
    )

    val currencyModelRub = CurrencyModel(rubCode, rubRate)

    val stubCurrencies = listOf(
        CurrencyModel(rubCode, 4.1),
        CurrencyModel(eurCode, 1.3),
        CurrencyModel(usdCode, 2.2)
    )

    val stubDomainModel = CurrenciesDomainModel(stubCurrencies)
}
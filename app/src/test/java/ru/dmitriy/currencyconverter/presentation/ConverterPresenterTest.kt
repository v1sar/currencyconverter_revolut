package ru.dmitriy.currencyconverter.presentation

import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import ru.dmitriy.currencyconverter.Stub
import ru.dmitriy.currencyconverter.domain.converter.ConverterInteractor
import ru.dmitriy.currencyconverter.presentation.converter.presenter.ConverterPresenter
import ru.dmitriy.currencyconverter.presentation.converter.presenter.TrampolineRule
import ru.dmitriy.currencyconverter.presentation.converter.view.ConverterView
import ru.dmitriy.currencyconverter.presentation.converter.view.`ConverterView$$State`
import org.mockito.Mockito.`when` as when_

@RunWith(MockitoJUnitRunner::class)
class ConverterPresenterTest {

    @Rule
    @JvmField
    val rule = TrampolineRule()

    @Mock
    lateinit var interactor: ConverterInteractor

    @Mock
    private lateinit var view: ConverterView

    private lateinit var presenter: ConverterPresenter

    @Mock
    private lateinit var viewState: `ConverterView$$State`

    @Before
    fun setUp() {
        presenter = ConverterPresenter(interactor)
        presenter.setViewState(viewState)
    }

    @Test
    fun `observingCurrenciesTest (call view)`() {
        when_(interactor.getCurrencies()).thenReturn(Observable.just(Stub.stubDomainModel))
        presenter.attachView(view)
        verify(viewState).showCurrencies(Stub.stubDomainModel.rates)
    }

    @Test
    fun `setLastCurrency (call view)`() {
        when_(interactor.getCurrencies()).thenReturn(Observable.just(Stub.stubDomainModel))
        presenter.attachView(view)
        presenter.setLastCurrency(Stub.stubCurrencies[0])
        verify(viewState, times(2)).showCurrencies(Stub.stubDomainModel.rates)
    }
}
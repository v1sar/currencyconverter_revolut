package ru.dmitriy.currencyconverter.domain

import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import ru.dmitriy.currencyconverter.Stub
import ru.dmitriy.currencyconverter.domain.converter.ConverterInteractor
import ru.dmitriy.currencyconverter.domain.converter.ConverterInteractorImpl
import ru.dmitriy.currencyconverter.domain.converter.ConverterRepository
import ru.dmitriy.currencyconverter.presentation.converter.presenter.TrampolineRule
import org.mockito.Mockito.`when` as when_


@RunWith(MockitoJUnitRunner::class)
class ConverterInteractorTest {

    @Rule
    @JvmField
    val rule = TrampolineRule()

    @Mock
    private lateinit var repository: ConverterRepository
    private lateinit var interactor: ConverterInteractor

    @Before
    fun setUp() {
        interactor = ConverterInteractorImpl(repository)
    }

    @Test
    fun `getCurrenciesTest (db)`() {
        when_(repository.getLastCurrencyModel()).thenReturn(Stub.currencyModelRub)
        when_(repository.getCurrenciesFromDb(Stub.rubCode)).thenReturn(Stub.listOfCurrenciesEntities)

        val currencies = interactor.getCurrencies()
            .blockingFirst()

        assertEquals(currencies.rates.size, 3)

        with(currencies.rates.get(0)) {
            assertEquals(Stub.rubCode, name)
            assertEquals(Stub.rubRate, rate, 0.1)
        }

        with(currencies.rates.get(1)) {
            assertEquals(Stub.eurCode, name)
            assertEquals(Stub.eurRate * Stub.rubRate, rate, 0.1)
        }

        with(currencies.rates.get(2)) {
            assertEquals(Stub.usdCode, name)
            assertEquals(Stub.usdRate * Stub.rubRate, rate, 0.1)
        }
    }

    @Test
    fun `getCurrenciesTest (network)`() {
        when_(repository.getLastCurrencyModel()).thenReturn(Stub.currencyModelRub)
        when_(repository.getCurrenciesFromDb(Stub.rubCode)).thenReturn(Stub.listOfCurrenciesEntities)
        when_(repository.getCurrenciesFromNetwork(Stub.rubCode)).thenReturn(Single.just(Stub.currencyResponseStub))

        val currencies = interactor.getCurrencies()
            .window(2)
            .blockingFirst()
            .blockingLast()

        assertEquals(currencies.rates.size, 3)

        with(currencies.rates.get(0)) {
            assertEquals(Stub.rubCode, name)
            assertEquals(Stub.rubRate, rate, 0.1)
        }

        with(currencies.rates.get(1)) {
            assertEquals(Stub.eurCode, name)
            assertEquals(Stub.eurRateNetwork * Stub.rubRate, rate, 0.1)
        }

        with(currencies.rates.get(2)) {
            assertEquals(Stub.usdCode, name)
            assertEquals(Stub.usdRateNetwork * Stub.rubRate, rate, 0.1)
        }
    }

    @Test
    fun `setLastCurrencyTest with RUB stub`() {
        interactor.setLastCurrency(Stub.stubCurrencies[0])
        verify(repository).setLastCurrencyModel(Stub.stubCurrencies[0])
    }

}